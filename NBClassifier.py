import pandas as pd
import numpy as np
from sklearn.naive_bayes import MultinomialNB
from sklearn import tree
from sklearn.model_selection import train_test_split

datafr = pd.DataFrame()
datafr = pd.read_csv('CleanedData.csv')




Y = datafr['state_code']
X = datafr.drop('state_code',axis =1)
X= np.array(X)
Y=np.array(Y)
X= np.nan_to_num(X)

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, stratify=Y,random_state=42)

c= MultinomialNB()
c.fit(x_train,y_train)
MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)

print(c.predict(X[2:3]))

train_pred = c.predict(x_train)
test_pred = c.predict(x_test)
accu_train = np.sum(train_pred== y_train )/float(len(y_train))
accu_test = np.sum(test_pred== y_test )/float(len(y_test))

print("Claasification accuracy on train: ",accu_train*100)
print("Claasification accuracy on test: ",accu_test*100)

submission = pd.DataFrame()
submission['actual_label_test'] = y_test
submission['predicted_label_test'] = test_pred


submission.to_csv('submissionNB.csv', index=False, sep=',', encoding='utf-8')