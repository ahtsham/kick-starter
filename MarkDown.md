Please follow the below steps

1- Run "PreProcessing.py" file
This will export the cleaned data into "CleanedData.csv" file

2- Run the "FeatureImp.py" file to see which feature is how much important before training the model. 
3- Run the "DecisionTree.py" file to train and test the model.
4- Now, run the second classifier "NBClassifier.py" you can see the output in the console.