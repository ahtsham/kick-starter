from sklearn import tree
import pandas as pd
from datetime import date
import numpy as np
import seaborn as sns
from sklearn.preprocessing import LabelEncoder

datafr = pd.DataFrame()
datafr = pd.read_csv('data.csv')
datafr = datafr.drop('ID', axis=1).drop('name', axis=1)
startdate = datafr['launched']
enddate = datafr['deadline']
datafr['start_date'] = [val.split(' ')[0] for val in startdate]
startdate = datafr['start_date']

datafr['start_date'] = pd.to_datetime(datafr['start_date'])
datafr['deadline'] = pd.to_datetime(datafr['deadline'])
datafr['days'] = (datafr['deadline'] - datafr['start_date']).dt.days
datafr = datafr.drop('start_date', axis=1).drop('deadline', axis=1).drop('launched',axis=1)
datafr = datafr[datafr.state != 'undefined']
datafr = datafr[datafr.state != 'suspended']
datafr['state'] = datafr['state'].replace('live', "successful")
datafr['state'] = datafr['state'].replace('canceled', "failed")

lb_categ = LabelEncoder()
datafr["Category_Code"] = lb_categ.fit_transform(datafr["category"])
lb_main_categ = LabelEncoder()
datafr["Main_Category_Code"] = lb_main_categ.fit_transform(datafr["main_category"])
lb_curr = LabelEncoder()
datafr["currency_code"] = lb_curr.fit_transform(datafr["currency"])
lb_state = LabelEncoder()
datafr["state_code"] = lb_state.fit_transform(datafr["state"])

lb_count = LabelEncoder()
datafr["country_code"] = lb_count.fit_transform(datafr["country"])

datafr = datafr.drop('category', axis=1).drop('main_category', axis=1).drop('currency',axis=1).drop('state',axis=1).drop('country',axis=1)




datafr.to_csv('CleanedData.csv',index =False)

